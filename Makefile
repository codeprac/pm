.ONESHELL:
SHELL = bash

verify:
	@cat ./development.code-workspace \
		| jq -r '.folders[].name' \
		| grep -v '^pm$$' \
		| xargs -I@ sh -c "stat -c '%F \"%n\" exists' ../@"

install:
	@if [ "$$(git diff --name-only HEAD | wc -l)" = "0" ]; then \
		go install \
			-ldflags "-X main.BuildDate=$$(date +'%Y.%m.%d') -X main.CommitSHA=$$(git rev-parse HEAD)" \
			./cmd/cpctl; \
	else
		go install \
			-ldflags "-X main.IsDev=true -X main.BuildDate=$$(date +'%Y.%m.%d')  -X main.CommitSHA=$$(git rev-parse HEAD)" \
			./cmd/cpctl; \
	fi
