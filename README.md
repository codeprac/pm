# Codeprac

This repository contains product/project management details for the Codeprac platform.

Project documentation can be found at https://www.notion.so/zephinzer/Codeprac-9bf7bf4377f14afaafb834346e881b98 (private page, make a request to @zephinzer for access)

# `cpctl`

Conventions are good. `cpctl` is a CLI helper tool for the technical team that is stored in this project and which implements the conventions and processes outlined in [this subpage](https://www.notion.so/zephinzer/Verbs-language-for-all-projects-7e6f976250734feb9b38f20af1fbbeee) of the above documentation.

To install it, you'll need Go to be installed on your machine. Once you have Go installed (run `go version` to verify), run:

```sh
make install;
```

You should then be able to verify it's been installed using:

```sh
cpctl --version;
```

## Initialising the development environment

```sh
cpctl setup;
```

## Install dependencies

```sh
cpctl install;
```

## Starting a project in development

```sh
cpctl start;
```

## Running linting in development

```sh
cpctl lint;
```

## Running tests in development

```sh
cpctl test;
```

## Building project in development

```sh
cpctl build;
```

## Tearing down the development environment

```sh
cpctl teardown;
```

## Viewing the status of the development environment

```sh
cpctl view devenv;
```

## Viewing the configuration

```sh
cpctl view configuration;
```
