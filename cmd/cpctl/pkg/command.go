package pkg

import (
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/zephinzer/go-devops"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "pkg",
		Aliases: []string{"package", "bundle"},
		Short:   "Creates the production package for testing locally",
		Long:    "Creates the production package for testing locally",
		RunE:    run,
	}
	return command
}

func run(cmd *cobra.Command, args []string) error {
	log.Debug("creating command...")
	workingDirectory, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("failed to get the working directory path: %s", err)
	}
	repo, err := git.PlainOpen(workingDirectory)
	if err != nil {
		return fmt.Errorf("failed to get git repository details: %s", err)
	}
	remotes, err := repo.Remotes()
	if err != nil {
		return fmt.Errorf("failed to get git remotes: %s", err)
	}

	var targetRemote *url.URL
	for _, remote := range remotes {
		if remote.Config().Name == "origin" {
			for _, u := range remote.Config().URLs {
				targetRemoteURL := strings.ReplaceAll(
					strings.ReplaceAll(
						strings.ReplaceAll(u, ".git", ""),
						":", "/",
					), "///", "://",
				)
				if !strings.Contains(targetRemoteURL, "://") {
					targetRemoteURL = "ssh://" + targetRemoteURL
				}
				log.Print(targetRemoteURL)
				if remoteURL, err := url.Parse(targetRemoteURL); err == nil {
					targetRemote = remoteURL
				}
			}
		}
	}
	if targetRemote == nil {
		targetRemote, _ = url.Parse("https://codepr.ac/default")
	}
	if command, err := devops.NewCommand(devops.NewCommandOpts{
		Command: "docker",
		Arguments: []string{
			"build",
			"--file",
			"./Dockerfile",
			"--tag",
			strings.Trim(targetRemote.Path, "/") + ":latest",
			".",
		},
		Flag: devops.CommandFlagset{
			UseGlobalEnvironment: true,
		},
	}); err != nil {
		return fmt.Errorf("failed to create command: %s", err)
	} else {
		log.Infof("executing command '%s'", command.String())
		if err := command.Run(); err != nil {
			return fmt.Errorf("failed to run command: %s", err)
		}
	}
	log.Info("command executed successfully")
	return nil
}
