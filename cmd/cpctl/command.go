package main

import (
	"strconv"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/codeprac/pm/cmd/cpctl/build"
	"gitlab.com/codeprac/pm/cmd/cpctl/denit"
	"gitlab.com/codeprac/pm/cmd/cpctl/initialise"
	"gitlab.com/codeprac/pm/cmd/cpctl/install"
	"gitlab.com/codeprac/pm/cmd/cpctl/lint"
	"gitlab.com/codeprac/pm/cmd/cpctl/pkg"
	"gitlab.com/codeprac/pm/cmd/cpctl/start"
	"gitlab.com/codeprac/pm/cmd/cpctl/test"
	"gitlab.com/codeprac/pm/cmd/cpctl/view"
)

var localConfig = config.Map{}

var (
	IsDev     string = "false"
	BuildDate string = "undefined"
	CommitSHA string = "dev"
)

func GetCommand() *cobra.Command {
	version := BuildDate + "-" + CommitSHA[:8]
	isDev, _ := strconv.ParseBool(IsDev)
	if isDev {
		version = "dev-" + version
	} else {
		version = version + " [https://gitlab.com/codeprac/pm/-/tree/" + CommitSHA[:8] + "]"
	}
	command := &cobra.Command{
		Use:     "cpctl",
		Short:   "Codeprac developer tool for development usage",
		Long:    "Codeprac developer tool for development usage",
		Version: version,
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
		SilenceUsage: true,
	}
	command.AddCommand(build.GetCommand())
	command.AddCommand(denit.GetCommand())
	command.AddCommand(initialise.GetCommand())
	command.AddCommand(install.GetCommand())
	command.AddCommand(lint.GetCommand())
	command.AddCommand(pkg.GetCommand())
	command.AddCommand(start.GetCommand())
	command.AddCommand(test.GetCommand())
	command.AddCommand(view.GetCommand())
	return command
}
