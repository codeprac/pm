package config

import (
	"github.com/spf13/cobra"

	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/codeprac/pm/cmd/cpctl/utils/config"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "config",
		Aliases: []string{"conf", "c"},
		Short:   "View the current configuration",
		Long:    "View the current configuration",
		RunE:    run,
	}
	return command
}

func run(cmd *cobra.Command, args []string) error {
	configurationAsYAML, err := config.Global.ToYAML()
	log.Print(configurationAsYAML)
	return err
}
