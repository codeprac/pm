package view

import (
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/pm/cmd/cpctl/view/config"
	"gitlab.com/codeprac/pm/cmd/cpctl/view/devenv"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "view",
		Aliases: []string{"check", "get", "see", "v"},
		Short:   "View information",
		Long:    "View information",
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
	}
	command.AddCommand(config.GetCommand())
	command.AddCommand(devenv.GetCommand())
	return command
}
