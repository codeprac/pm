package main

import (
	"os"

	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/codeprac/pm/cmd/cpctl/utils/config"
)

func main() {
	if err := config.LoadGlobal(); err != nil {
		log.Errorf("failed to load global configuration: %s", err)
		os.Exit(1)
	}
	if err := GetCommand().Execute(); err != nil {
		log.Errorf("failed to execute successfully: %s", err)
		os.Exit(2)
	}
}
