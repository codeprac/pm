package initialise

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/zephinzer/go-devops"
)

var conf = config.Map{
	"restart": &config.Bool{
		Shorthand: "r",
		Usage:     "when this flag is specified, development environment will be restarted instead",
	},
}

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "init",
		Aliases: []string{"initialise", "setup", "up", "u"},
		Short:   "Provisions the development environment if any",
		Long:    "Provisions the development environment if any",
		RunE:    run,
	}
	conf.ApplyToCobra(command)
	return command
}

func run(cmd *cobra.Command, args []string) error {
	log.Debug("creating command...")
	newCommandArgs := []string{
		"--file",
		"./docker-compose.yml",
	}
	newCommandOperation := "up"
	if conf.GetBool("restart") {
		newCommandOperation = "restart"
	}
	newCommandArgs = append(newCommandArgs, newCommandOperation)
	newCommandArgs = append(newCommandArgs, "-d", "--remove-orphans")
	if command, err := devops.NewCommand(devops.NewCommandOpts{
		Command:   "docker-compose",
		Arguments: newCommandArgs,
		Flag: devops.CommandFlagset{
			UseGlobalEnvironment: true,
		},
	}); err != nil {
		return fmt.Errorf("failed to create command: %s", err)
	} else {
		log.Infof("executing command '%s'", command.String())
		if err := command.Run(); err != nil {
			return fmt.Errorf("failed to run command: %s", err)
		}
	}
	log.Info("command executed successfully")
	return nil
}
