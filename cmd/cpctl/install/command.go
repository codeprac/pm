package install

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/zephinzer/go-devops"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "install",
		Aliases: []string{"ins", "deps"},
		Short:   "Pulls dependencies into the current project",
		Long:    "Pulls dependencies into the current project",
		RunE:    run,
	}
	return command
}

func run(cmd *cobra.Command, args []string) error {
	log.Debug("determining type of project...")
	var commandOpts devops.NewCommandOpts
	if isGo, err := devops.IsProjectType(".", devops.TypeGo); err != nil {
		return fmt.Errorf("failed to determine type of project: %s", err)
	} else if isGo {
		log.Debug("installing dependencies for go project...")
		commandOpts = getGoCommandOpts()
	} else if isJavascript, err := devops.IsProjectType(".", devops.TypeJavascript); err != nil {
		return fmt.Errorf("failed to determine type of project: %s", err)
	} else if isJavascript {
		log.Debug("installing dependencies for js project...")
		commandOpts = getJavascriptCommandOpts()
	}

	log.Debug("creating command...")
	if command, err := devops.NewCommand(commandOpts); err != nil {
		return fmt.Errorf("failed to create command: %s", err)
	} else {
		log.Infof("executing command '%s'", command.String())
		if err := command.Run(); err != nil {
			return fmt.Errorf("failed to run command successfully: %s", err)
		}
	}
	log.Info("command executed successfully")
	return nil
}

func getGoCommandOpts() devops.NewCommandOpts {
	return devops.NewCommandOpts{
		Command: "go",
		Arguments: []string{
			"mod",
			"vendor",
		},
		Flag: devops.CommandFlagset{
			UseGlobalEnvironment: true,
		},
	}
}

func getJavascriptCommandOpts() devops.NewCommandOpts {
	return devops.NewCommandOpts{
		Command:   "npm",
		Arguments: []string{"install"},
		Flag: devops.CommandFlagset{
			UseGlobalEnvironment: true,
		},
	}
}
