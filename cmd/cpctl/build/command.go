package build

import (
	"fmt"
	"io/ioutil"
	"path"
	"runtime"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/zephinzer/go-devops"
)

var localConf config.Map = config.Map{
	"target-arch": &config.String{
		Usage:     "defines the target architecture (if applicable)",
		Shorthand: "a",
	},
	"target-os": &config.String{
		Usage:     "defines the target OS (if applicable)",
		Shorthand: "o",
	},
}

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "build",
		Aliases: []string{"b"},
		Short:   "Builds the current project",
		Long:    "Builds the current project",
		RunE:    run,
	}
	localConf.ApplyToCobra(command)
	return command
}

func run(cmd *cobra.Command, args []string) error {
	log.Debug("determining type of project...")
	var commandOpts devops.NewCommandOpts
	var isGo, isJavascript bool
	var err error
	if isGo, err = devops.IsProjectType(".", devops.TypeGo); err != nil {
		return fmt.Errorf("failed to determine type of project: %s", err)
	} else if isGo {
		log.Debug("building go project...")
		commandOpts, err = getGoCommandOpts()
	} else if isJavascript, err = devops.IsProjectType(".", devops.TypeJavascript); err != nil {
		return fmt.Errorf("failed to determine type of project: %s", err)
	} else if isJavascript {
		log.Debug("building js project...")
		commandOpts = getJavascriptCommandOpts()
	}
	if err != nil {
		return fmt.Errorf("failed to get command handler: %s", err)
	}

	log.Debug("creating command...")
	if command, err := devops.NewCommand(commandOpts); err != nil {
		return fmt.Errorf("failed to create command: %s", err)
	} else {
		log.Infof("executing command '%s'", command.String())
		if err := command.Run(); err != nil {
			return fmt.Errorf("failed to run command successfully: %s", err)
		}
	}
	log.Info("command executed successfully")
	return nil
}

func getGoCommandOpts() (devops.NewCommandOpts, error) {
	targetArchitecture := localConf.GetString("target-arch")
	targetOS := localConf.GetString("target-os")
	targetCmd := ""
	cmds, _ := ioutil.ReadDir("./cmd")
	if len(cmds) != 1 {
		return devops.NewCommandOpts{}, fmt.Errorf("failed to identify target command")
	}
	targetCmd = cmds[0].Name()

	var goos, goarch string
	if targetOS != "" {
		goos = targetOS
	} else {
		goos = runtime.GOOS
	}
	if targetArchitecture != "" {
		goarch = targetArchitecture
	} else {
		goarch = runtime.GOARCH
	}

	targetBin := targetCmd
	if goos == "windows" {
		targetBin += ".exe"
	}

	return devops.NewCommandOpts{
		Command: "go",
		Arguments: []string{
			"build",
			"-o",
			"./" + path.Join("bin/", targetBin),
			"./" + path.Join("cmd/", targetCmd),
		},
		Environment: map[string]string{
			"GOOS":   goos,
			"GOARCH": goarch,
		},
		Flag: devops.CommandFlagset{
			UseGlobalEnvironment: true,
		},
	}, nil
}

func getJavascriptCommandOpts() devops.NewCommandOpts {
	return devops.NewCommandOpts{
		Command: "npm",
		Arguments: []string{
			"run",
			"build",
		},
		Flag: devops.CommandFlagset{
			UseGlobalEnvironment: true,
		},
	}
}
