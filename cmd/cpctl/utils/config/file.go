package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// File defines the structure of the configuration file
type File struct {
	location   string  `json:"-" yaml:"-"`
	AWSProfile *string `json:"awsProfile" yaml:"awsProfile"`
	Env        *string `json:"env" yaml:"env"`
}

// ToJSON converts this File instance into a JSON string
func (f File) ToJSON() (string, error) {
	configurationAsJSON, err := json.Marshal(f)
	if err != nil {
		return "", err
	}
	return string(configurationAsJSON), nil
}

// ToYAML converts this File instance into a YAML string
func (f File) ToYAML() (string, error) {
	configurationAsJSON, err := yaml.Marshal(f)
	if err != nil {
		return "", err
	}
	return string(configurationAsJSON), nil
}

// Save writes the data in this File instance into a file
func (f File) Save() error {
	if configurationAsYAML, err := yaml.Marshal(f); err != nil {
		return fmt.Errorf("failed to marshal configuration into yaml: %s", err)
	} else if err := ioutil.WriteFile(globalPath, configurationAsYAML, os.ModePerm); err != nil {
		return fmt.Errorf("failed to write configuration to file: %s", err)
	}
	return nil
}
