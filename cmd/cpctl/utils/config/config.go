package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/zephinzer/go-devops"
	"gopkg.in/yaml.v2"
)

var (
	globalPath = "~/.codeprac"
)

var Global *File

// init loads the global codeprac path
func init() {
	globalPath, _ = devops.NormalizeLocalPath(globalPath)
	Global = &File{}
}

// Load returns a config.File instance ideally;
// when no arguments are passed in, it defaults to loading the
// configuration from the globalPath variable which is a file
// in the user's home directory named ".codeprac". To change the
// target configuration file, specify the `alternatePath`
// argument
func Load(alternatePath ...string) (*File, error) {
	var err error
	targetPath := globalPath
	if len(alternatePath) > 0 {
		targetPath, err = devops.NormalizeLocalPath(alternatePath[0])
		if err != nil {
			return nil, fmt.Errorf("failed to normalize provided path '%s': %s", alternatePath[0], err)
		}
	}
	if _, err = os.Lstat(targetPath); err != nil {
		newConfiguration := File{location: targetPath}
		if errors.Is(err, os.ErrNotExist) {
			configurationData, _ := yaml.Marshal(newConfiguration)
			if err := os.WriteFile(targetPath, configurationData, os.ModePerm); err != nil {
				return nil, fmt.Errorf("failed to initialise configuration file at '%s': %s", targetPath, err)
			}
		} else {
			return nil, fmt.Errorf("failed to access file at '%s': %s", targetPath, err)
		}
		return &newConfiguration, nil
	}
	existingConfiguration := File{}
	if configurationData, err := ioutil.ReadFile(targetPath); err != nil {
		return nil, fmt.Errorf("failed to read file at '%s': %s", targetPath, err)
	} else if err := yaml.Unmarshal(configurationData, &existingConfiguration); err != nil {
		return nil, fmt.Errorf("failed to unmarshal configuration data from binary into via yaml: %s", err)
	}
	existingConfiguration.location = targetPath
	return &existingConfiguration, nil
}

// LoadGlobal is a convenience method for loading the global
// configuration; this is equivalent to calling Load() without
// any arguments and saving the configuration to config.Global
func LoadGlobal() (err error) {
	Global, err = Load()
	return err
}
